# snowyrocker

Dockerfile's for connecting to Snowflake from R with development environments in [RStudio Server](https://www.rstudio.com/products/rstudio/#rstudio-server).

## Setup

1. Clone this repo somewhere:

```sh
git clone https://gitlab.com/jeremyyeo/snowyrocker.git
```

2. [OPTIONAL] Add a `.env` file with your Snowflake connection details in the root of the repository:

```
SNOWFLAKEUSER=<snowflake-username>
SNOWFLAKEPASSWORD=<snowflake-password>
SNOWFLAKESERVER=<snowflake-account-identifier>.<cloud-region>.snowflakecomputing.com
SNOWFLAKEWAREHOUSE=<warehouse-name>
SNOWFLAKEROLE=<role-name>
SNOWFLAKEDATABASE=<database-name>
```

> Note: The above environment variables are used in the example `example.R` script.

2. Build the container locally from the root directory:

```sh
cd snowyrocker
docker build -t snowyrocker rstudio/.
```

3. Run the container from the root directory:

```sh
docker run --env-file ./.env -d -p 8787:8787 -v $(pwd):/home/rstudio -e PASSWORD=mypassword snowyrocker
```

> Note: the above docker run instructions will mount the root directory `snowyrocker/` so that it is visible when you are connected to RStudio Server from the browser.
> This means that any file you create while operating within RStudio Server from the container will also be available on the host machine.

4. Open up a browser and navigate to `localhost:8787` and login using `rstudio` as the username and `mypassword` as the password.
   Test out your connection to Snowflake using the `example.R` script which should be visible in the "Files" pane on the bottom right panel of the RStudio UI.

## Shiny app development

You can develop Shiny apps directly via the RStudio Server UI, simply open the `clock-app/app.R` file and run the app:

![Demo of working shiny app](images/dev-shiny-apps.gif)
